# control variables

lists <- NULL
nums <- NULL

#data basepaths
srcdata_path <- paste0(getwd(),"/source_data/",source_version,"/")

# load country list
countries <- 
  read.csv2(file = 
              paste0(srcdata_path,"countries.csv"),
            row.names = NULL, check.names = F)

lists$countries <-  countries$country.source
nums$countries <- length(lists$countries)
# append sectors
lists$sectors <- sectors$sector.source
nums$sectors <- length(lists$sectors)

# identify rows
rows <- data.frame(country = rep(lists$countries, each = nums$sectors))
rows$sector <- lists$sectors

rows$productive <- sectors$productive
rows$num_country <- match(rows$country, countries$country.source)
rows$num_sector <- match(rows$sector, sectors$sector.source)
rows$country_sector <- paste0(rows$country,".",rows$sector)

nums$countries_sectors <- nums$countries*nums$sectors

# load demands list
demands <-
  read.csv2(paste0(srcdata_path,"demand.csv"))
nums$demands <- dim(demands)[1]

# identify columns
columns <- data.frame(country = c(rows$country, 
                               rep(lists$countries, each = nums$demands)))
columns$sector <- c(rows$sector, rep(demands$demand, times = nums$countries))
columns$num_country <- match(columns$country, countries$country.source)
columns$country_sector <- paste0(columns$country,".",columns$sector)

# Define parameters lists
lists$years <- names(sea_source[,1,1,1])
lists$input <- rows$country_sector
lists$output <- columns$country_sector
nums$years <- length(lists$years)
nums$input <- length(lists$input)
nums$output <- length(lists$output)

